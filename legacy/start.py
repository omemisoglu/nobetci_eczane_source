import re
import sys
import os
import time
import subprocess
import pathlib
import datetime
import sched
import shlex
import pygit2

import logger as lg
from scripter import Sh_Script_Write
import webdrive as wd
import conf as cf
import file_manager as fm
import gif_maker as gifm
import html_lib
import tr_string as trstr
##########
# MACROS #
##########

DEBUG = 0

# Check for debug argument
#
if len(sys.argv) > 1:
    for i in sys.argv: 
        if i == 'debug':
            DEBUG = 1
            break
        
# Get absolute log path
#
DIR_LOG = os.path.realpath('.log')

#############
# VARIABLES #
#############

script_self_exe = ''
script_main_exe = 'main'

if DEBUG:
    dbg_start = time.time()

# Check Frozen state
#
if getattr(sys, 'frozen', False):
    # running in a bundle
    script_self_abspath = sys.executable
    script_self_exe = ''
else:
    # running live
    script_self_abspath = os.path.realpath(__file__)
    script_self_exe = 'python3 '
    script_main_exe += '.py'
    
script_head, script_tail = os.path.split(script_self_abspath)

# Get the absolute path of the executables respectively of 
# their state (frozen or live)
#
script_main_exe = script_self_exe + os.path.join(script_head, script_main_exe)
script_self_exe += os.path.join(script_head, script_tail)

if DEBUG:
    print('Self Executable: ', script_self_exe)
    print('Main Executable: ', script_main_exe)

driver_path_chrome = ''

if sys.platform.startswith('win32'):
    driver_path_chrome = 'drivers/win10/chromedriver.exe'
elif sys.platform.startswith('linux'):
    driver_path_chrome = 'drivers/raspi/chromedriver'

# Dependant files and folders list
#
file_dep_list = [
                 '.conf/settings.conf',
                 'drivers/raspi/chromedriver',
                 'html/resources/eflash.gif',
                 'html/resources/get_date.js',
                 'html/resources/styles.css',
                 'html/resources/3d_bg.svg',
                 'html/resources/nulshock_bd.ttf',
                 'html/template/html_template.html',
                 'html/template/gif_template.html'
                 ]
folder_dep_list = ['database',
                   'html']
# Temporary folders list
#
folder_temp_list = ['.temp', '.log']

SPLASH_PATH = os.path.realpath(os.path.join('html','resources','splash.png'))

##############
# FUNCTIONS  #
##############

def Debug_Call(msg):
    if DEBUG:
        print(msg)

def Version_Check():
    ans = False
    # Get the current branch
    #
#    result = subprocess.run(['git', 'rev-parse', '--abbrev-ref', 'HEAD'],   \
#                        stdout=subprocess.PIPE)
#    curr_branch = result.stdout.decode().strip()
    repo = pygit2.Repository(pygit2.discover_repository(os.getcwd()))
    curr_branch = repo.head.shorthand
    
    Debug_Call("Curr Branch: " + curr_branch)
    
    ver_base = ''
    if (curr_branch == 'master'):
        ver_base = 'r'
    else:
        ver_base = 'd'
#    org_branch = 'origin/' + curr_branch
    
    # Get the current version
    #
#    result = subprocess.run(['git', 'tag', '--list', '--sort=-taggerdate', \
#                             '--points-at='+org_branch],                 \
#                            stdout=subprocess.PIPE)
    result = subprocess.run(['git', 'describe', '--tags','--abbrev=0'],      \
                            stdout=subprocess.PIPE)
    
    git_out = result.stdout.decode()
    ver_current = re.findall(r'\d+.\d+', git_out)

    # Fetch tags if new version tag is available
    #
    result = subprocess.run(['git', 'fetch', 'origin', curr_branch, '--tags'],
                            stdout=subprocess.PIPE)
    
    # Get the last version
    #
    result = subprocess.run(['git', 'tag', '--list', '--sort=-taggerdate'],   \
                            stdout=subprocess.PIPE)
    git_out = result.stdout.decode()
    ver_last = re.findall(ver_base + r'\d+.\d+', git_out)

    if (not(len(ver_last) and len(ver_current))):
        lg.Exception_Log(DIR_LOG, 'No version tag found!!!')
        sys.exit(1)
        
    Debug_Call("Curr Version: " + ver_current[0])
    Debug_Call("Remote Version: " + ver_last[0])

    # If version is higher than the current, return 'True' to update
    #
    if (float(ver_last[0].replace(ver_base, '')) > float(ver_current[0])):
        ans = True
        Debug_Call("New Version available!")
    
    return ans
    
def Version_Update(cwdir, driver):
    repo = pygit2.Repository(pygit2.discover_repository(os.getcwd()))
    curr_branch = repo.head.shorthand
    
    os.chdir('../')
    
    drv_pid = str(driver.service.process.pid)
    
    t_str = "ps -o ppid= -o pid= -A | awk '$1 == " + drv_pid +   \
            "{print $2}'"
    pid_lines = ['#! /bin/bash',
                 'sleep 3s',
                 'currentscript="$0"',
                 'function finish {',
                 'shred -u ${currentscript};',
                 '}',
                 t_str,
                 'sleep 1s',
                 'trap finish EXIT'
                ]
    cmd_pid = Sh_Script_Write('get_pid.sh', pid_lines)
    result = subprocess.run(cmd_pid, stdout=subprocess.PIPE)
    drv_cid = result.stdout.decode()
    
    Debug_Call('cid is: ' + str(drv_cid))
    Debug_Call('pid is: ' + str(cmd_pid))
    
    scr_lines = ['#! /bin/bash',
                 'sleep 5s',
                 'currentscript="${0##*/}"',
                 'args="$@"',
                 'function finish {',
                 script_self_exe + ' ${args}' + ';',
                 '}',
                 'cd ' + cwdir,
                 'git pull origin ' + curr_branch,
                 'sleep 5s',
                 'trap finish EXIT']
    
    upt_exe = Sh_Script_Write('update_rep.sh', scr_lines)
    if DEBUG:
        upt_exe += ' debug'
    upt_exe += ' ' + drv_pid + ' ' + drv_cid
    
    time.sleep(5)
    with open(os.devnull, 'r+b', 0) as DEVNULL:
        subprocess.Popen(shlex.split(upt_exe),    \
                         stdin=DEVNULL, stdout=DEVNULL,  \
                         stderr=subprocess.STDOUT,       \
                         close_fds=True)
    time.sleep(2)
    sys.exit()

def Schedule_Create(time_list, idex=None, interval=3):
    if (int(interval) <= 0):
        interval = 1
    dt = datetime.datetime.now()
    tm_schedule = None
    if (idex != None):
        if (idex == 0):
            dt += datetime.timedelta(days=1)
        dt_shift = datetime.datetime.strptime(time_list[idex], '%H.%M').replace( \
                   year=dt.year, month=dt.month, day=dt.day)
        dt_str = str(dt_shift.year) + '-' + str(dt_shift.month) + '-' +   \
                 str(dt_shift.day) + ' ' + time_list[idex]
        tm_schedule = time.mktime(time.strptime(dt_str,'%Y-%m-%d %H.%M'))
        return tm_schedule, ((idex + 1) % len(time_list))
    
    idex = 0
    dt_save = dt + datetime.timedelta(minutes=interval)
    dt_str_hour = str(dt_save.hour) + '.' + str(dt_save.minute)
    
    for i in time_list:
        idex = time_list.index(i)
        dt_shift = datetime.datetime.strptime(i, '%H.%M').replace(    \
                            year=dt.year, month=dt.month, day=dt.day)
        dt_dif = dt_shift - dt
        dt_dif_abs = abs (dt_shift - dt)
        if (dt_dif_abs.seconds < interval * 60):
            dt_str_hour =  str(dt_shift.hour) + '.' + str(dt_shift.minute)
            break
        elif (dt_dif.days >= 0):
            dt_str_hour = i
            break
        elif (time_list.index(i) >= (len(time_list) - 1)):
            ii = time_list[0]
            dt_shift = datetime.datetime.strptime(ii, '%H.%M').replace(       \
                        year=dt.year, month=dt.month, day=dt.day)
            dt_shift += datetime.timedelta(days=1)
            dt_str_hour = ii
    
    dt_str = str(dt_shift.year) + '-' + str(dt_shift.month) + '-' +   \
             str(dt_shift.day) + ' ' + dt_str_hour
    tm_schedule = time.mktime(time.strptime(dt_str,'%Y-%m-%d %H.%M'))
    
    return tm_schedule, ((idex + 1) % len(time_list))

def Is_Duty(hour_op='8.30', hour_cl='18.15'):
    res = True
    dt = datetime.datetime.now()
    dt_opening = datetime.datetime.strptime(hour_op,            \
                 '%H.%M').replace(year=dt.year, month=dt.month,\
                 day=dt.day)
    dt_closing = datetime.datetime.strptime(hour_cl, \
                 '%H.%M').replace(year=dt.year, month=dt.month,\
                 day=dt.day)
    dt_dif_op = dt_opening - dt
    dt_dif_cl = dt_closing - dt
    Debug_Call('Difference from opening: '+ str(dt_dif_op))
    Debug_Call('Difference from closing: '+ str(dt_dif_cl))
    
    # If the time is in DUTY hours
    #
    if ((dt_dif_op.days < 0) and (dt_dif_cl.days >= 0)):
        res = False
    return res

def Is_Holiday(date=None):
    from workalendar.europe import Turkey

    ans = False

    if date == None:
        date = datetime.datetime.today()
    cal = Turkey()
    
    ans = True if date.weekday() == 6 else False

    ans_list = [True for i in cal.holidays(date.year) if i[0] == date.date()]
    ans = True if len(ans_list) or ans else False
    
    return ans

def Ad_HTML_Img_Vid(img_dir):
    if (os.path.isdir(img_dir) == False):
        return '', ''
    img_list = []
    vid_list = []
    for file_name in os.listdir(img_dir):
        if file_name.endswith('.png') or    \
           file_name.endswith('.jpg') or    \
           file_name.endswith('.jpeg') or    \
           file_name.endswith('.gif') or    \
           file_name.endswith('.svg'):    \
            img_list.append(os.path.realpath(os.path.join(img_dir, file_name)))
        elif file_name.endswith('.mp4'):
            vid_list.append(os.path.realpath(os.path.join(img_dir, file_name)))
    
    img_out = ''
    vid_out = ''
    
    if len(vid_list) + len(img_list):
        if len(img_list):
            for i in img_list:
                img_out += 'String.raw`' + i + '`'
                if img_list.index(i) < len(img_list) - 1:
                    img_out += ', \n'
        if len(vid_list):
            for i in vid_list:
                vid_out += 'String.raw`' + i + '`'
                if vid_list.index(i) < len(vid_list) - 1:
                    vid_out += ', \n'
    
    return img_out, vid_out

def Ad_HTML_Create(html_name, html_template, img_str, vid_str, img_key, vid_key):
     gif_page = fm.Read_My_File(html_template, encoding='tr')
     gif_page = gif_page.replace(img_key, img_str).replace(vid_key, vid_str)
     html_path = html_lib.Create_HTML_Page(gif_page, html_name + '.html')
     return html_path

def Splash_HTML_Create():
    # Keys in HTML Template
    #
    html_keys = ['<!--HTML_Body-->',
         '<!--HTML_Pharmacy-->',
         '<!--HTML_Address-->',
         '<!--HTML_Number-->',
         '<!--HTML_QR-->',
         '<!--HTML_QR_Info-->',
         '<!--HTML_Footer-->',
         '<!--HTML_End-->'
        ]
    phar_name = trstr.TR_Case_Change(PHAR_NAME, case='upper')
    
    # Read HTML Templates
    #
    os.chdir('html/template')
    html_out = html_lib.Read_HTML_Template('html_template.html', html_keys)
    os.chdir('../../')
    
    ecz_holder = 'CELL_ECZANELER_HOLDER'
    caps_holder = 'CAPS_ECZANE'
    
    holder_list = ['CELL_ADRESLER_HOLDER'   ,
                   'CELL_TELEFONLAR_HOLDER' ,
                   'CELL_QR_HOLDER'         ,
                   'CELL_QR_INFO_HOLDER'    ,
                   'INFO_NOTE_HOLDER'       ,
                   'INFO_DISC_HOLDER'
                  ]
        
    html_start = html_out[0]
    html_body  = html_out[1]
    html_phar  = html_out[2]
    html_footer = html_out[7]
    html_end    = html_out[8]
    
    html_start = html_start.replace('ECZANE(LER)', 'ECZANELER')              \
                           .replace('logo_holder', PHAR_CITY_NR + '_logo')   \
                           .replace('<color_holder>', THEME_COLOR)

    html_phar = html_phar.replace('eczane_label', 'eczane_off_duty')         \
                         .replace('eczane_holder',                           \
                                  phar_name + r'<br/>SAĞLIKLI GÜNLER DİLER') \
                         .replace('NR_ECZANELER', '1')               \
                         .replace('<LAST>', 'none')

    html_body = html_body.replace(ecz_holder, html_phar)
    html_footer = html_footer.replace(caps_holder, phar_name)

    html_page = html_start + html_body + html_footer + html_end
    for i in holder_list:
       html_page = html_page.replace(i,'')
    html_path = html_lib.Create_HTML_Page(html_page, 'splash.html')
    
    return html_path

##########
def Gif_HTML_Create(img_path, gif_name, html_temp):
    gif_page = fm.Read_My_File(html_temp, encoding='tr')
    gif_page = gif_page.replace('GIF_HOLDER', img_path)
    gif_html_path = html_lib.Create_HTML_Page(gif_page, gif_name+'.html')
    return gif_html_path

def Splash_Show(driver, cwdir, gif_name, path):
    html_path = Ad_HTML_Create(os.path.join(cwdir, path), gif_name,
                          'html/template/gif_template.html')
    wd.Chrome_Open_Page(driver, html_path)
    return html_path
##########
    
def My_Waiter():
    return

########
# MAIN #
########
if __name__ == '__main__':

    # Local Variables
    #
#    first_launch = True
    page_active = 'about:blank'
    html_ad_name = 'screensaver'
    gif_name = 'screensaver'
    # Define the update times in a list
    #
    time_list = ['8.30', '9.00', '9.30', '10.00','12.00', '17.00',
                 '17.30','18.00', '18.25','18.31','19.00']
    idex_schedule = None
    
    # Run TODO.py
    #
    path = pathlib.Path('.temp/todo.py')
    if path.exists() == True:
        subprocess.run(shlex.split('python3 ".temp/todo.py"'))
        time.sleep(1)
        subprocess.run(shlex.split('rm ".temp/todo.py"'))

    # Set working directory to git root folder
    #
    result = subprocess.run(['git', 'rev-parse', '--show-toplevel'],    \
                            stdout=subprocess.PIPE)
    dir_root = result.stdout.decode().strip()
    
    # Open the browser fullscreen
    #
    driver = wd.Chrome_Init(driver_path_chrome)
    time.sleep(1)
#    page_active = Splash_Show(driver, dir_root, gif_name, SPLASH_PATH)
    
    # Kill the processes from last run
    #
    if len(sys.argv) > 1:
        idx_args = 0
        for i in sys.argv:
            if (idx_args != 0) and (i.isdigit() == True):
                subprocess.run(['kill', '-9', str(i)])
            idx_args += 1
    
    # Check necessary files for the program to run
    #
    for i in file_dep_list:
         path = pathlib.Path(i)
         if path.exists() == False:
             os._exit()

    # Create folders necessary for the program to run
    #
    for i in folder_temp_list:
        if (os.path.isdir(i) == False):
            os.mkdir(i)
    
    # Read Configurations
    #
    confs = cf.Config_Get(".conf/settings.conf", 'tr')
    PHAR_NAME = confs.get('user', 'name')
    PHAR_CITY_NR = confs.get('user', 'city')
    THEME_COLOR = confs.get('graphics', 'theme')
    
    # Read whether ads are shown off-duty or not
    #
    is_ads = bool(confs.getboolean('graphics','ads'))
    
    # Loop forever
    #
    while True:
        try:
            os.chdir(dir_root)
            ##### 1. UPDATE CHECK #####
            Debug_Call('-> Version_Check()')
            is_update = Version_Check()
            Debug_Call('<- Version_Check()')
            if (is_update == True):
                Debug_Call('-> Version_Update()')
                Version_Update(dir_root, driver)
                Debug_Call('<- Version_Update()')

            ##### 2. RUN MAIN #####
            # Run the main program in subprocess call
            #
            Debug_Call('-> main()')
            proc = subprocess.run(shlex.split(script_main_exe),         \
                                  stdout=subprocess.PIPE,              \
                                  stdin=subprocess.PIPE,               \
                                  stderr=subprocess.PIPE, shell=False)
            Debug_Call('<- main()')
            
            Debug_Call('Returncode: '+ str(proc.returncode))
            
            # Read the location of the HTML page to be displayed
            #
            html_path = proc.stdout.decode().strip('\n')
            Debug_Call('HTML Path: ' + html_path)
            
            is_schedule = True
            
            # If the time is in DUTY hours
            #
            if (Is_Duty() or not(is_ads) or Is_Holiday()):
                if proc.returncode >= 10:
                    is_schedule = False
                    time.sleep(20)
                elif proc.returncode < 10:
                    page_active = html_path
            else:
                img_out, vid_out = Ad_HTML_Img_Vid('resimler')
                
                if (img_out != '') and (vid_out != ''):
                    html_ad_path = Ad_HTML_Create(html_ad_name,
                                          'html/template/gif_template.html',
                                          r''+img_out, r''+vid_out, 
                                          'IMG_HOLDER', 'VID_HOLDER')
                    page_active = html_ad_path
                else:
                    page_active = Splash_HTML_Create()
            
            # Refresh the page
            #
            wd.Chrome_Open_Page(driver, page_active)
            
            if is_schedule == True:
                # Arrange the schedule by time list
                #
                tm_schedule, idex_schedule = Schedule_Create(time_list, 
                                                         idex_schedule, 10)
                
                # Create the schedule module to run the function
                #
                s = sched.scheduler(time.time, time.sleep)
                s.enterabs(tm_schedule, 0, My_Waiter)
                s.run()
            
            if DEBUG == 1:
                print('Elapsed time: ', time.time() - dbg_start)
            
                
        except Exception:
            lg.Exception_Log(DIR_LOG)
            time.sleep(60)
            pass
#            g_driver.quit()
#            os.system('sudo reboot -r now')
