# -*- coding: utf-8 -*-
import sys
import os
import locale
#import re

import datetime
import pathlib
import requests
from bs4 import BeautifulSoup
import urllib
import pandas

# Custom imports
#
import file_manager as fm
import html_lib
import logger as lg
import qr_maker as qrm
import string_manager as strm
import tr_string as trstr
import conf as cf

#####################
# Macros & Defines  #
#####################
PHAR_DIST = None
DIR_LOG = os.path.realpath('.log')

###############################################
# System Platform Dependant Configurations    #
###############################################

headers_var = {}

if sys.platform.startswith('win32'):
    headers_var = {
            "User-Agent": 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) '
            'AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 '
            'Safari/537.36'
            }
elif sys.platform.startswith('linux'):
    headers_var = {
            "User-Agent": 'Mozilla/5.0 (X11; Linux armv7l) '
            'AppleWebKit/537.36 (KHTML, like Gecko) '
            'Raspbian Chromium/72.0.3626.121 '
            'Chrome/72.0.3626.121 Safari/537.36'
            }

##############
# Functions  #
##############

# Read all pharmacy information from first sheet
#
def Phar_DB_Read(city_code, filtre=None, dir_file='database'):
    cwdir = os.getcwd()
    os.chdir(dir_file)
    excel_path = city_code + '_eczaneler.xlsx'
    table = pandas.read_excel(excel_path) #sheet_name=None
    os.chdir(cwdir)
    if (filtre != None):
        table = table.loc[table.iloc[:,1] == filtre]
    return table
    
##############################################################################
def URL_Encoder(url):
     return urllib.parse.quote(url, encoding='ASCII')

def Maps_Geo_to_Encoded_URL(geolocation):
     maps_url_base = 'https://www.google.com/maps/search/?api=1&query='
     return URL_Encoder(maps_url_base + geolocation)
 
def Encoded_URL_to_Google_QR(url):
    # Variables for QR URL Generation
    #
    qr_url_head = 'https://chart.googleapis.com/chart?cht=qr&chl='
    qr_url_body = url
    qr_url_end = '&chs=180x180&choe=UTF-8&chld=L|2'
    return qr_url_head + qr_url_body + qr_url_end
##############################################################################

#############################################################################
#####################
# Global Variables  #
#####################

# Set locale of the program to TR
#
locale.setlocale(locale.LC_ALL, "tr_TR")

# Dependant files and folders list
#
file_dep_list = ['.conf/settings.conf',
                 'html/template/html_template.html',
                 'html/resources/eflash.gif',
                 'html/resources/get_date.js',
                 'html/resources/styles.css']
folder_dep_list = ['database', 
                   '.conf']

# Temporary folders list
#
folder_temp_list = ['.temp', '.log']
# Open the browser fullscreen first thing
#
#g_driver = Chrome_Init(driver_path_chrome)

# Path of HTML Page to be generated and shown in page
#
html_name = 'nobetci_eczaneler.html'
html_path = 'file://' + os.path.join(os.getcwd(), 'html', html_name)
print(html_path)

## Read the configurations from settings file
##
#confs_xml = fm.Read_My_File('.conf/settings.conf')
#confs_soup = BeautifulSoup(confs_xml)
#THEME_COLOR = confs_soup.graphics.theme.contents[0]
#confs_list = Conf_Read('settings.txt', 'utf-8')
confs = cf.Config_Get(".conf/settings.conf", 'tr')

PHAR_CITY_NR = confs.get('user', 'city')
PHAR_NAME = confs.get('user', 'name')
PHAR_DIST = confs.get('user', 'district')
THEME_COLOR = confs.get('graphics', 'theme')

# Read all pharmacy information from first sheet
#
DB_TABLE = Phar_DB_Read(PHAR_CITY_NR, filtre=PHAR_DIST)
duty_list_memory = pandas.DataFrame(columns=DB_TABLE.columns)

# Get only pharmacies on configured district
#
#if (PHAR_DIST != None):
#    DB_TABLE = DB_TABLE.loc[DB_TABLE.iloc[:,1] == PHAR_DIST]
    
# Read Cities with their plate
#
city_name_list = pandas.read_csv('database/plaka.csv', encoding='iso-8859-9', 
                          index_col=False, dtype={1: str})

# Get the city name from plate number taken from configuration file
#
city_name_list = city_name_list.loc[city_name_list.iloc[:,1] == PHAR_CITY_NR]
CITY_NAME = city_name_list.iloc[0,0]

# Set variables depends on City
#
URL = ''
attrs_0 = {}
attrs_1 = {}
time_opening = '08.30' + "'a"

if (PHAR_CITY_NR == '39'):
    # Define website to be collected
    #
    URL = 'https://www.kirklarelieo.org.tr/nobetci-eczaneler'
    
    # Set HTML Element attributes according to city
    #
    attrs_0 = {"class": "eleven columns bottom-1"}
    attrs_1 = {"class": "eight columns top-1"}
    
elif (PHAR_CITY_NR == '22'):
    # Define website to be collected
    #
    URL = 'https://www.edirneeo.org.tr/nobetci-eczaneler'
    
    # Set HTML Element attributes according to city
    #
    attrs_0 = {"class": "twelve columns bottom-1"}
    attrs_1 = {"class": "nine columns top-1"}
    

########
# MAIN #
########
if __name__ == '__main__':
    try:
        # Create Eczane Ad and Eczane Ilce Lists
        #
        eczane_ad_web = []
        eczane_ilce_web = []
        
        # Get the launch day of the program
        # Then get the next day
        #
        dt_today = datetime.datetime.today()
        dt_today_time = float(dt_today.hour) + (float(dt_today.minute) / 100.0)
        
        dt_tomorrow = dt_today + datetime.timedelta(days=1)
        tomorrow_str = datetime.datetime.strftime(dt_tomorrow, '%d %B %Y')
        
        # Check necessary files for the program to run
        #
#        for i in file_dep_list:
#             path = pathlib.Path(i)
#             if path.exists() == False:
        if not(fm.Files_Check(file_dep_list)):
            sys.exit(22)
        
        # Create folders necessary for the program to run
        #
#        for i in folder_temp_list:
#            if (os.path.isdir(i) == False):
#                os.mkdir(i)
        fm.Folders_Check(folder_temp_list, create=True)
        
        ##### 1. Extraction of Pharmacies from Website ######
        
        # 1.1 Get website content with url and headers
        #
        page = requests.get(URL, headers=headers_var, timeout=10.0)
        
        # Parse content with module bs4 via html parser
        #
        soup = BeautifulSoup(page.content, 'html.parser')
        
        # Find all classes of eleven columns bottom-1. This class contains the 
        # information per pharmacy: Name, District, Address and Phone
        #
        eczaneler = soup.findAll("div", attrs_0)
        
        for i in eczaneler:
            # 1.2 Find Pharmacy Names
            
            # Find the class in page specified below containing Pharmacy Name in class 
            # <strong> and other informations in the next class <p> as District, 
            # Address and Phone
            #   
            t_div = i.find("div", attrs_1)
            
            # If there is a class available inside of the parent class (for loop base)
            #
            if ((t_div != -1) and (t_div != None)):
                # Find the class <p>
                #
                t_div = t_div.find("p")
                if((t_div != -1) and (t_div != None)):
                    # Find the class <strong> and read its content as text (Phar. Name)
                    #
                    t_text_pre = t_div.find("strong").text
                    
                    # Save the name of the phar. splitted by only 1 space between 
                    # words to the list of phar. names received from website !
                    #
                    t_text = strm.Purge_Space(t_text_pre)
                    if (t_text == None):
                        sys.exit(33)
                    eczane_ad_web.append(t_text)
                    
                    ##-------------------------------------------------------##
                    
                    # 1.3 Find Pharmacy Districts
                    
                    # Find the next sibling class <p> in parent class of <eight...><p>
                    #
                    t_div = t_div.findNextSibling("p")
                    
                    # Split the other informations by new line to District, Address
                    # and Phone and clone them into the information list
                    #
                    t_bilgi_list = t_div.text.split("\n")
                    
                    # Save the district splitted by only 1 space between 
                    # words to the list of districts received from website.
                    #
                    ilce_web = strm.First_Words(t_bilgi_list)
                    if (ilce_web == None):
                        sys.exit(44)
                    eczane_ilce_web.append(ilce_web)
                    
        # Retrieve pharmacy names of lists
        #
        xls_eczane_adlar = DB_TABLE.iloc[:, 0]
        
        # 1.3 Create list of pharmacies on duty
        #
        duty_list = pandas.DataFrame(columns=DB_TABLE.columns)

        ###### 2. Compare Pharmacy Names from Website and XLS Database #######
        
        # Writing them in a pandas dataframe
        #
        idx_eczane_ad_web = 0
        for i in eczane_ad_web:
            i_temp = trstr.TR_to_EN(trstr.TR_Case_Change(i))
            ilce_temp = trstr.TR_Case_Change(eczane_ilce_web[idx_eczane_ad_web])
            idx_eczane_ad_web += 1
#            ilce_temp = ilce_temp.capitalize()
            for j in xls_eczane_adlar:
                j_temp = trstr.TR_to_EN(trstr.TR_Case_Change(str(j)))
                
                if ((j_temp == i_temp) and (ilce_temp == trstr.TR_Case_Change(PHAR_DIST))):
                    tempo = DB_TABLE.loc[DB_TABLE.iloc[:,0] == j]
                    tempo = tempo.reset_index(drop=True)
                    
                    # If our pharmacy is on duty, put us on the top
                    #
                    if ((PHAR_NAME == str(tempo.iloc[0, 0])) and 
                        (PHAR_DIST == str(tempo.iloc[0, 1]))):
                        duty_list.loc[-1] = tempo.iloc[0,:]
                        duty_list.index += 1
                        duty_list = duty_list.sort_index()
                    else:
                        # Append the found pharmacy in duty list
                        #
                        duty_list = duty_list.append(tempo, ignore_index = True)
                    break
        
#        # Get the last duty_list from .temp folder
#        #
#        path = pathlib.Path('.temp/dataframe.csv')
#        if path.exists() == True:
#            duty_list_memory = pandas.read_csv('.temp/dataframe.csv', encoding='iso-8859-9')
#        
#        # If duty list has not changed, do not create HTML and 
#        # wait the next schedule
#        # 
#        if (duty_list.equals(duty_list_memory) == True):
#            sys.exit(1)
#        elif (len(duty_list) > 0):
#            # Save the actual duty_list to the CSV file to retrieve back later
#            #
#            duty_list.to_csv('.temp/dataframe.csv', encoding='iso-8859-9', \
#                             index_label=False)
#        else:
#            # If no pharmacy has been found, return the code error.
#            #
#            sys.exit(2)
                    
        # Check if we have a duty list
        #
        if (len(duty_list) > 0):
            os.chdir(".temp")
            duty_mem_files = [f for f in pathlib.Path.cwd().iterdir() \
                              if f.match("dataframe_*.csv")]
            # Check if we have last of a duty list
            #
            if (len(duty_mem_files)):
                duty_list_memory = pandas.read_csv(duty_mem_files[0].name, encoding='iso-8859-9')
                
                if (duty_list.equals(duty_list_memory) == True):
                    duty_mem_date_str = duty_mem_files[0].name              \
                                        .replace("dataframe_", "")          \
                                        .replace(".csv","")
                    duty_mem_date = datetime.datetime.strptime(duty_mem_date_str, 
                                                               '%Y-%m-%d')             \
                                    .replace(hour=12, minute=00)
                    dt = datetime.datetime.now()
                    dt_dif = duty_mem_date - dt
                    
                    if dt_dif.days >= 0:
                        sys.exit(1)
                
                # Remove the last duty list
                #
                for i in duty_mem_files:
                    os.remove(i)
            
            # Save the actual duty_list to the CSV file to retrieve back later
            #
            duty_list.to_csv('dataframe_' + str(dt_tomorrow.date()) + '.csv', 
                             encoding='iso-8859-9', index_label=False)
            os.chdir("..")
        else:
            # If no pharmacy has been found, return the code error.
            #
            sys.exit(11)

        ###### 3. HTML Web Page Generation ##########
        
        # HTML Generation Contents
        #
        html_start = ''
        html_body = ''
        html_footer = ''
        html_end = ''
        
        # Temporary variables
        #
        temp_html_body = ''
        html_body_template = ''
        
        # Pharmacy Information holders in HTML Template
        #
        eczane_holder = 'eczane_holder'
        adres_holder = 'adres_holder'
        telefon_holder = 'telefon_holder'
        qr_holder = 'qr_holder'
        foot_eczaneler = ''
        
        # String Holders in HTML Body
        #
        body_ec_holder = 'CELL_ECZANELER_HOLDER'
        body_ad_holder = 'CELL_ADRESLER_HOLDER'
        body_tel_holder = 'CELL_TELEFONLAR_HOLDER'
        body_qr_holder = 'CELL_QR_HOLDER'
        body_qr_info_holder = 'CELL_QR_INFO_HOLDER'
        
        footer_note_holder = 'INFO_NOTE_HOLDER'
        footer_note_temp = '''ECZANELER DATE sabah saat SAAT_HOLDER kadar 
        nöbetçidir.<br/>MY_ECZANE sağlıklı günler diler.'''
        
        footer_disc_holder = 'INFO_DISC_HOLDER'
        footer_disc_temp = '''Nöbetçi eczane bilgileri city_holder 
                            Eczacı Odasının internet sitesinden alınmıştır.'''
        
        # Keys in HTML Template
        #
        html_keys = ['<!--HTML_Body-->',
             '<!--HTML_Pharmacy-->',
             '<!--HTML_Address-->',
             '<!--HTML_Number-->',
             '<!--HTML_QR-->',
             '<!--HTML_QR_Info-->',
             '<!--HTML_Footer-->',
             '<!--HTML_End-->'
            ]
        
        # Read HTML Templates
        #
        os.chdir('html/template')
        html_out = html_lib.Read_HTML_Template('html_template.html', html_keys)
        os.chdir('../../')
        
        # Sections in HTML Template
        #
        html_start          = html_out[0]
        html_body_template  = html_out[1]
        html_eczane         = html_out[2]
        html_address        = html_out[3]
        html_telefon        = html_out[4]
        html_qr             = html_out[5]
        html_qr_info        = html_out[6]
        html_footer         = html_out[7]
        html_end            = html_out[8]
        
        # Clean old QR Codes from the folder if directory exists
        #
        if (os.path.isdir('html/resources/qr') == True):
            fm.Clean_Folder('html/resources/qr')
        else:
            # Create directory
            #
            os.chdir('html/resources')
            os.mkdir('qr')
            os.chdir('../../')
            
        # Replace HTML Title if only one pharmacy is on duty
        #
        if (not(len(duty_list) > 1)):
            html_start = html_start.replace('(LER)', '')
        else:
            html_start = html_start.replace('(LER)', 'LER')
        
        # Replace Logo respectively to the City Pharmacy Union 
        # and color theme according to XML Configuration File
        #
        logo_name = PHAR_CITY_NR + '_logo'
        html_start = html_start.replace('logo_holder', logo_name)   \
                               .replace('<color_holder>', THEME_COLOR)

        # Replace the HTML Element Holders with the found pharmacies in duty
        #
        t_html_ec = ''
        t_html_ad = ''
        t_html_tel = ''
        t_html_qr = ''
        t_html_qr_info = ''
        
        for i, row in duty_list.iterrows():
            tt_ec = ''
            tt_ad = ''
            tt_tel = ''
            tt_qr = ''
            tt_qr_info = ''
            
            if ((row[0] == PHAR_NAME) and (row[1] == PHAR_DIST)):
                t_qr_holder = ''
                t_eczane_holder = '<div class="my_eczane">ECZANEMİZ</div>'
                t_telefon_holder = ''
                t_adres_holder = ''
                
            else:
                t_qr_holder = qrm.Data_to_QR(row[5], i)
                t_qr_holder = '<img src="' + t_qr_holder + '" class="qr_img">'
                t_eczane_holder = row[0]
                t_telefon_holder = str(row[2])
                t_adres_holder = row[3]
            
            t_footer = row[0]
            if ((len(duty_list) > 1) and (len(duty_list) - 1 <= i)):
                foot_eczaneler += ' ve ' + t_footer
            elif (len(duty_list) > 2 and (len(duty_list) - 2 > i)):
                foot_eczaneler += t_footer + ', '
            else:
                foot_eczaneler += t_footer
            #qr_temp = Encoded_URL_to_Google_QR(Maps_Geo_to_Encoded_URL(row[4]))
            
            
            if(i >= len(duty_list) - 1):
                tt_ec = html_eczane.replace('<LAST>','none')
                tt_ad = html_address.replace('<LAST>','none')
                tt_tel = html_telefon.replace('<LAST>','none')
                tt_qr = html_qr.replace('<LAST>','none')
                tt_qr_info = html_qr_info.replace('<LAST>','none')
            else:
                tt_ec = html_eczane.replace('<LAST>','solid')
                tt_ad = html_address.replace('<LAST>','solid')
                tt_tel = html_telefon.replace('<LAST>','solid')
                tt_qr = html_qr.replace('<LAST>','solid')
                tt_qr_info = html_qr_info.replace('<LAST>','solid')
            
            t_html_ec       += tt_ec.replace(eczane_holder, t_eczane_holder)
            t_html_ad       += tt_ad.replace(adres_holder, t_adres_holder)
            t_html_tel      += tt_tel.replace(telefon_holder, t_telefon_holder)
            t_html_qr       += tt_qr.replace(qr_holder, t_qr_holder)
            t_html_qr_info  += tt_qr_info
        t_html_ad = t_html_ad.replace('No: ', 'No:&nbsp;')               \
                             .replace(' Sok:', '&nbsp;Sok:')
        
        html_body = html_body_template.replace(body_ec_holder, t_html_ec)  \
                        .replace(body_ad_holder, t_html_ad)                \
                        .replace(body_tel_holder, t_html_tel)              \
                        .replace(body_qr_holder, t_html_qr)                \
                        .replace(body_qr_info_holder, t_html_qr_info)

        html_body = html_body.replace('NR_ECZANELER',str(len(duty_list)))
        
        # Replace footer holders for proper information and append HTML Footer 
        #
        html_footer = html_footer.replace(footer_disc_holder, footer_disc_temp)
        html_footer = html_footer.replace(footer_note_holder, footer_note_temp)
        html_footer = html_footer.replace('ECZANELER', foot_eczaneler)
        html_footer = html_footer.replace('DATE', tomorrow_str)
        html_footer = html_footer.replace('MY_ECZANE', PHAR_NAME)
        html_footer = html_footer.replace('CAPS_ECZANE', 
                                          trstr.TR_Case_Change(PHAR_NAME, 
                                                               case='upper'))
        html_footer = html_footer.replace('city_holder', CITY_NAME)
        html_footer = html_footer.replace('SAAT_HOLDER', time_opening)

        html_body += html_footer
        
        # Finalize the HTML page content by storing it into a single variable
        #
        html_page = html_start + html_body + html_end
        
        html_lib.Create_HTML_Page(html_page, html_name)
        sys.exit(0)
        
    except (requests.ConnectionError, requests.ConnectTimeout, \
            requests.exceptions.ConnectionError, requests.RequestException):
        lg.Exception_Log(DIR_LOG)
        sys.exit(10)
    except Exception as e:
        print('|||' + str(e) + '|||')
        lg.Exception_Log(DIR_LOG)
        sys.exit(999)
        
