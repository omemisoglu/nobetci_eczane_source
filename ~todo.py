# -*- coding: utf-8 -*-

import os, shlex, sys, subprocess, time
from pathlib import Path


def Untrack_File(file_path):
    subprocess.run(shlex.split('git update-index --assume-unchanged ' + file_path))
    return

def Sh_Script_Write(scr_name, scr_lines):
    path = Path(scr_name)
    if path.exists() == True:
        subprocess.run(['rm', scr_name])
        
    f=open(scr_name, 'w')
    for i in scr_lines:
        f.write(i+'\n')
    f.close()
    
    subprocess.run(['chmod', '+x', scr_name])
    return (os.path.abspath(scr_name))

# Pass browser arguments
#
def Args_Get(arg_list):
    args_str = ''    
    if len(arg_list) > 1:
        idx_args = 0
        for i in arg_list:
            if (idx_args != 0) and (i.isdigit() == True):
                args_str += str(i)
                idx_args += 1
                if (idx_args < len(arg_list)):
                    args_str += ' '
            else:
                idx_args += 1
    return args_str
    
args_str = Args_Get(sys.argv)

# Get working directory of git root folder
#
def Git_Root():
    result = subprocess.run(['git', 'rev-parse', '--show-toplevel'],    \
                        stdout=subprocess.PIPE)
    cwdir = result.stdout.decode().strip()
    return cwdir

def Async_Run(cmd):
    with open(os.devnull, 'r+b', 0) as DEVNULL:
        subprocess.Popen(shlex.split(cmd),    \
                         stdin=DEVNULL, stdout=DEVNULL,  \
                         stderr=subprocess.STDOUT,       \
                         close_fds=True)
    time.sleep(1)
    return

def Sync_Run(cmd):
    subprocess.run(shlex.split(cmd))
    return
    
# Script Write and Run in the same working directory Example
#
#scr_lines = ['#! /bin/bash',
#             'sleep 15s',
#             'currentscript="${0##*/}"',
#             'DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"',
#             'args="$@"',
#             'function finish {',
#             'cd DIR',
#             'shred -u ${currentscript};',
#             '}',
#             'cd ' + cwdir,
#             'git update-index --assume-unchanged '+ os.path.realpath(__file__),
#             'rm ' + os.path.realpath(__file__),
#             'trap finish EXIT']
#scr_path = os.path.abspath('todo_del.sh')
#abs_pt = Sh_Script_Write(scr_path, scr_lines)
#Async_Run(abs_pt)

os.chdir(Git_Root())
Untrack_File('.temp/todo.py')
