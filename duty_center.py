# -*- coding: utf-8 -*-
"""
Created on Wed Oct 16 17:44:12 2019

@author: omemi
"""
import sys
import os
import time
from typing import NamedTuple

import datetime
import pathlib
import requests
from bs4 import BeautifulSoup
import pandas
from PIL import Image
# Custom imports
#
#import logger as lg
import file_manager as fm
import html_lib
import qr_maker as qrm
import string_manager as strm
import tr_string as trstr
import dict_manager as dctm
from geodist import Geo_Distance

#from dataclasses import dataclass

class Pharmacy(NamedTuple):
    name : str
    city_code : str
    city_name : str
    dist_name : str

class Screen:
    path = str
    active = str
    prev = str
    theme = str
    is_saver = bool

    def __init__(self, path, theme, is_saver):
        if type(path) != str or type(theme) != str or type(is_saver) != bool:
            return None

        self.path = path
        self.theme = theme
        self.is_saver = is_saver

class Webpage:
    url = ""
    attr_list = []
    header = ""

    def __init__(self, city_code, header):
        if type(header) != dict:
            return None

        # Set variables depends on City
        #
        if (city_code == '39'):
            # Define website to be collected
            #
            URL = 'https://www.kirklarelieo.org.tr/nobetci-eczaneler'

            # Set HTML Element attributes according to city
            #
            ATTR_LIST = [
                            {"class": "eleven columns bottom-1"},
                            {"class": "eight columns top-1"}]

        elif (city_code == '22'):
            URL = 'https://www.edirneeo.org.tr/nobetci-eczaneler'
            ATTR_LIST = [
                          {"class": "twelve columns bottom-1"},
                          {"class": "nine columns top-1"}]

        self.url = URL
        self.attr_list = ATTR_LIST
        self.header = header

    def extract(self):

        # Define Output Lists of Phar. Names and Districts
        #
        eczane_ad_web = []
        eczane_ilce_web = []

        # 1.1 Get website content with url and headers
        #
        page = requests.get(self.url, headers=self.header, timeout=10.0)

        # Parse content with module bs4 via html parser
        #
        soup = BeautifulSoup(page.content, 'html.parser')

        # Find all classes of eleven columns bottom-1. This class contains the
        # information per pharmacy: Name, District, Address and Phone
        #
        eczaneler = soup.findAll("div", self.attr_list[0])

        for i in eczaneler:
            # 1.2 Find Pharmacy Names

            # Find the class in page specified below containing Pharmacy Name in class
            # <strong> and other informations in the next class <p> as District,
            # Address and Phone
            #
            t_div = i.find("div", self.attr_list[1])

            # If there is a class available inside of the parent class (for loop base)
            #
            if ((t_div != -1) and (t_div != None)):
                # Find the class <p>
                #
                t_div = t_div.find("p")
                if((t_div != -1) and (t_div != None)):
                    # Find the class <strong> and read its content as text (Phar. Name)
                    #
                    t_text_pre = t_div.find("strong").text

                    # Save the name of the phar. splitted by only 1 space between
                    # words to the list of phar. names received from website !
                    #
                    t_text = strm.Purge_Space(t_text_pre)
                    if (t_text == None):
                        sys.exit(33)
                    eczane_ad_web.append(t_text)

                    ##-------------------------------------------------------##

                    # 1.3 Find Pharmacy Districts

                    # Find the next sibling class <p> in parent class of <eight...><p>
                    #
                    t_div = t_div.findNextSibling("p")

                    # Split the other informations by new line to District, Address
                    # and Phone and clone them into the information list
                    #
                    t_bilgi_list = t_div.text.split("\n")

                    # Save the district splitted by only 1 space between
                    # words to the list of districts received from website.
                    #
                    ilce_web = strm.First_Words(t_bilgi_list)
                    if (ilce_web == None):
                        sys.exit(44)
                    eczane_ilce_web.append(ilce_web)
        ans = [eczane_ad_web, eczane_ilce_web]
        return ans

class DutyCenter:
    pharmacy:       Pharmacy
    mode:           str
    database:       pandas.DataFrame
    duty_list:      pandas.DataFrame
    duty_stamp:     datetime.datetime
    screen:         Screen
    webpage:        Webpage

    map_prefix =    'init'

    def __init__(self, pharmacy, screen, header, mode):
        self.pharmacy = pharmacy
        self.database = self.read_database(filtre=self.pharmacy.dist_name) #database
        self.duty_list = pandas.DataFrame(columns=self.database.columns)
        self.mode = mode
        self.screen = screen

        self.webpage = Webpage(pharmacy.city_code, header)
    # Read all pharmacy information from first sheet
    #
    def read_database(self, filtre=None, dir_file='database'):

        excel_path = os.path.join(dir_file, self.pharmacy.city_code + '_eczaneler.xlsx')
        table = pandas.read_excel(excel_path) #sheet_name=None

        if (filtre != None):
            table = table.loc[table.iloc[:,1] == filtre]
        return table

    def comp_get_duty(self, web_info_list):
        ans = False

        # Retrieve pharmacy names of lists
        #
        xls_eczane_adlar = self.database.iloc[:, 0]

        # Create list of pharmacies on duty based on database
        #
        duty_list = pandas.DataFrame(columns=self.database.columns)

        # Writing them in a pandas dataframe
        #
        idx_list = 0
        for i in web_info_list[0]:
            # Pharmacy name fetched from website
            #
            i_temp = trstr.TR_to_EN(trstr.TR_Case_Change(i))
            ilce_temp = trstr.TR_Case_Change(web_info_list[1][idx_list])
            idx_list += 1
            for j in xls_eczane_adlar:
                # Pharmacy name in database
                #
                j_temp = trstr.TR_to_EN(trstr.TR_Case_Change(str(j)))

                if (((j_temp.find(i_temp) != -1) or (i_temp.find(j_temp) != -1)) and
                    (ilce_temp == trstr.TR_Case_Change(self.pharmacy.dist_name))):

                    tempo = self.database.loc[self.database.iloc[:,0] == j]
                    tempo = tempo.reset_index(drop=True)

                    # If our pharmacy is on duty, put us on the top
                    #
                    if ((self.pharmacy.name == str(tempo.iloc[0, 0])) and
                        (self.pharmacy.dist_name == str(tempo.iloc[0, 1]))):
                        duty_list.loc[-1] = tempo.iloc[0,:]
                        duty_list.index += 1
                        duty_list = duty_list.sort_index()
                    else:
                        # Append the found pharmacy in duty list
                        #
                        duty_list = duty_list.append(tempo, ignore_index = True)
                    break
        if len(duty_list):
            self.duty_list = duty_list.copy(deep=True)
            self.duty_stamp = datetime.datetime.now()
            ans = True

        return ans

    def check_expired(self, duty_mem_files):
        is_expired = True

        # If we have last of a duty list
        #
        if (len(duty_mem_files)):
            duty_list_memory = pandas.read_csv(duty_mem_files[0],
                                               encoding='iso-8859-9')
            if not(len(duty_list_memory)):
                return True

            if not(len(self.duty_list)):    # FATAL ERROR TODO: Think something!!!
                is_expired = False          # Change this error code

            # Sort the pharmacies
            #
            columns = self.duty_list.columns
            duty_list_sorted = self.duty_list.sort_values(by=[columns[0],
                                                            columns[3]])  \
                                    .reset_index(drop=True)
            columns = duty_list_memory.columns
            duty_list_memory_sorted = duty_list_memory.sort_values(by=[columns[0],
                                                                    columns[3]]) \
                                                    .reset_index(drop=True)

            # If the last duty list is the same as current one
            # check the expiration date of the last list
            #
            if (duty_list_sorted.equals(duty_list_memory_sorted) == True):
                duty_mem_date_str = duty_mem_files[0].name              \
                                    .replace("dataframe_", "")          \
                                    .replace(".csv","")
                duty_mem_date = datetime.datetime.strptime(duty_mem_date_str,
                                                        '%Y-%m-%d')        \
                                .replace(hour=12, minute=00)
                dt = datetime.datetime.now()
                dt_dif = duty_mem_date - dt

                # If it didn't expired, do nothing
                #
                if dt_dif.days >= 0:
                    is_expired = False

        return is_expired

    def manage_memory(self, tomorrow, path_dir='.temp'):

        # Find saved duty list(s) in the target folder
        #
        duty_mem_files = [f for f in pathlib.Path(path_dir).iterdir() \
                          if f.match("dataframe_*.csv")]
        duty_mem_files.sort(reverse=True)

        # Create Memory Template
        #
#        duty_list_memory = pandas.DataFrame(columns=self.duty_list.columns)

        is_expired = self.check_expired(duty_mem_files)

        if is_expired:
            for i in duty_mem_files:
                os.remove(i)
            self.save_duty(tomorrow, path_dir)

        return is_expired

    def save_duty(self, dt_tomorrow, path_dir='.temp'):

        # Save the actual duty_list to the CSV file to retrieve back later
        #
        path_save = os.path.join(path_dir,
                        'dataframe_' + str(dt_tomorrow.date()) + '.csv')
        self.duty_list.to_csv(path_save , encoding='iso-8859-9', index_label=False)

    def generate_map(self, webdriver, index, org, dest,
                     dir_out=os.path.join('html', 'resources', 'map')):

        ### Generate database name of the map
        #
        str_0 = self.pharmacy.city_code
        str_1 = trstr.TR_to_EN(self.pharmacy.dist_name.replace(' ',''),'lower')
        str_2 = trstr.TR_to_EN(self.duty_list.iloc[index, 0]        \
                                   .replace(' Eczanesi', '')        \
                                   .replace(' ', ''),               \
                               'lower')
        map_db_name = str_0 + '_' + str_1 + '_' + str_2 + '.png'
        map_db_path = os.path.abspath(os.path.join(dir_out, map_db_name))

        ### Open the website
        # If map is not loaded more than 3 times, use offline maps
        #
        break_counter = 0
        map_loaded = False
        while not(map_loaded):
            if break_counter > 3:
                return map_db_path

            # Preprocess geolocations
            #
            org_list = strm.Split_Full(org, ',')
            dest_list = strm.Split_Full(dest, ',')

            # Get geolocation distance
            #
            geo_dist = Geo_Distance((float(org_list[0]), float(org_list[1])), (float(dest_list[0]), float(dest_list[1])))
            geo_dist = round(geo_dist, 2) * 1000

            # If geolocation distance is shorter than 500m use walk route
            #
            if geo_dist > 500:
                engine = 'fossgis_osrm_car'
            else:
                engine = 'fossgis_osrm_foot'

            webdriver.open('https://www.openstreetmap.org/directions?engine=' +
                           engine + '&route=' + org + ';' + dest)
            time.sleep(10)

            # Check if map elements are loaded
            #
            class_list = ['leaflet-tile-pane',
                          'leaflet-shadow-pane',
                          'leaflet-overlay-pane']
            map_loaded = webdriver.check_condition(class_list)

            break_counter  += 1
        time.sleep(10)

        # Save website temporarily and disappear the browser
        #
        t_path_img = os.path.abspath(os.path.join('.temp', 'map_tmp.png'))
        webdriver.driver.set_window_position(-2000, -2000)
        webdriver.driver.minimize_window()
        time.sleep(5)
        webdriver.screenshot(t_path_img)
        webdriver.driver.minimize_window()

        ### Crop website to the map with fixed size
        #
        img = Image.open(t_path_img)
        width, height = img.size
        right = width - 40
        bottom = height - 22
        left= right - 880 if width > 880 else 0
        top = bottom - 660 if height > 660 else 0
        img_map = img.crop((left, top, right, bottom))

        # Add OSM Banner
        #
        banner = Image.open(os.path.join('html', 'resources', 'osm-banner.png'))
        palette = Image.new('RGBA', img_map.size)
        palette.paste(banner, (img_map.width - banner.width, \
                               img_map.height - banner.height), mask=banner)
        img_out = Image.alpha_composite(img_map, palette)

        # Save map to the HTML resources folder
        #
#        path_out = os.path.abspath(os.path.join(dir_out, str_map))
#        img_out.save(path_out, format="PNG")

        # Save to map database
        #
        img_out.save(map_db_path, format="PNG")

        # Remove the temporary image
        #
        os.remove(t_path_img)

        # Return the path to the map
        #
        return map_db_path

    def generate_ad_screen(self, img_dir, path_out, path_template):
        if (os.path.isdir(img_dir) == False):
            return None

        img_list = []
        vid_list = []
        for file_name in os.listdir(img_dir):
            if file_name.endswith('.png') or    \
               file_name.endswith('.jpg') or    \
               file_name.endswith('.jpeg') or    \
               file_name.endswith('.gif') or    \
               file_name.endswith('.svg'):    \
                img_list.append(os.path.realpath(os.path.join(img_dir, file_name)))
            elif file_name.endswith('.mp4'):
                vid_list.append(os.path.realpath(os.path.join(img_dir, file_name)))

        img_out = ''
        vid_out = ''

        if len(vid_list) + len(img_list):
            if len(img_list):
                for i in img_list:
                    img_out += 'String.raw`' + i + '`'
                    if img_list.index(i) < len(img_list) - 1:
                        img_out += ', \n'
            if len(vid_list):
                for i in vid_list:
                    vid_out += 'String.raw`' + i + '`'
                    if vid_list.index(i) < len(vid_list) - 1:
                        vid_out += ', \n'

            # Ad Screen from HTML Template
            #
            img_key = 'IMG_HOLDER'
            vid_key = 'VID_HOLDER'
            html_page = fm.Read_My_File(path_template, encoding='tr')
            html_page = html_page.replace(img_key, img_out).replace(vid_key, vid_out)
            html_out = html_lib.Create_HTML_Page(html_page, path_out)
        else:
            html_out = None

        return html_out

    def generate_splash_screen(self, path_out, path_template):
        # Splash Screen from HTML Template
        #
        PHAR_CITY_NR = self.pharmacy.city_code
        CITY_NAME = self.pharmacy.city_name
        PHAR_NAME = self.pharmacy.name

        if self.mode == "eczane":
            PHAR_NAME_PRINT = PHAR_NAME
        else:
            PHAR_NAME_PRINT = " ".join([CITY_NAME, "Eczacı Odası"])
        THEME_COLOR = self.screen.theme

        html_temp = html_lib.Read_HTML_Template(path_template, 'tr')
        eczane_caps = trstr.TR_Case_Change(PHAR_NAME_PRINT, case='upper')
        replacements = {'LOGOH': PHAR_CITY_NR + '_logo',
                        'COLORH': THEME_COLOR,
                        'CAPSH': eczane_caps
                        }
        for key, value in replacements.items():
            dctm.subdict_update(html_temp, key, 'value', value)

        html_page = html_lib.Fill_Templates(html_temp)
        html_path = html_lib.Create_HTML_Page(html_page, path_out)
        return html_path

    def generate_duty_screen(self, file_path, webdriver):
        ### Input Variables
        duty_list = self.duty_list

        PHAR_CITY_NR = self.pharmacy.city_code
        CITY_NAME = self.pharmacy.city_name
        PHAR_NAME = self.pharmacy.name

        if self.mode == "eczane":
            PHAR_NAME_PRINT = PHAR_NAME
        else:
            PHAR_NAME_PRINT = " ".join([CITY_NAME, "Eczacı Odası"])
        PHAR_DIST = self.pharmacy.dist_name
        THEME_COLOR = self.screen.theme

        PHAR_GEOLOC = self.database.loc[self.database.iloc[:,0] == PHAR_NAME]
        PHAR_GEOLOC =  PHAR_GEOLOC.loc[PHAR_GEOLOC.iloc[:,1] == PHAR_DIST].iloc[0,4]

        dt_tomorrow = self.duty_stamp + datetime.timedelta(days=1)
        tomorrow_str = datetime.datetime.strftime(dt_tomorrow, '%d %B %Y')
        time_opening = '08.30' + "'a"

        dir_list = [os.path.join('html/resources/qr')]
        for i in dir_list:
            # Clean old QR Codes from the folder if directory exists
            #
            if ((os.path.isdir(i) == True) and (i.find('map') == -1)):
                fm.Clean_Folder(i)
            else:
                # Create directory
                #
                os.mkdir(i)

        # Read HTML Template
        #
        html_temp_dict = html_lib.Read_HTML_Template(os.path.join('html',
                                                                  'template',
                                                        'html_template.html'),
                                                     'tr')

        if (not(len(duty_list) > 1)):
            t_ler = ''
        else:
            t_ler = 'LER'

        def Change_Nrecz(dic, key):
            dic[key]['value'] = str(len(duty_list))

        dctm.subdict_update(html_temp_dict, '_LER_', 'value', t_ler)
        dctm.subdict_update(html_temp_dict, 'LOGOH', 'value', PHAR_CITY_NR + '_logo')
        dctm.subdict_update(html_temp_dict, 'COLORH', 'value', THEME_COLOR)
        dctm.dict_findall(html_temp_dict, 'NRECZ', Change_Nrecz)

        # Determine to the map prefix
        #
        if self.map_prefix == 'init' or self.map_prefix == 'beta':
            self.map_prefix = 'alpha'
        else:
            self.map_prefix = 'beta'

        my = 0
        # For every pharmacy in duty list
        #
        for i, row in duty_list.iterrows():

            # If we are on duty
            #
            if ((row[0] == PHAR_NAME) and (row[1] == PHAR_DIST)):
                t_ecz = '<div class="my_eczane">ECZANEMİZ</div>'
                t_adr = ' '
                t_tel = ' '
                t_qr = ' '
                t_map = ''
                dctm.subdict_update(html_temp_dict, 'QRINFOHOLDER','value', ' ')
                dctm.subdict_update(html_temp_dict, 'IDHOLDER', 'value', 'nan')
                my = 1
            else:
                t_ecz = row[0]
                t_adr = row[3].replace('No: ', 'No:&nbsp;').replace(' Sok.', '&nbsp;Sok.')
                t_tel = str(row[2])
                t_qr = '<img src="' + qrm.Data_to_QR(row[5], i) + '" class="qr_img">'

                t_map = self.generate_map(webdriver, i, PHAR_GEOLOC, row[4])
                t_map = 'String.raw`' + t_map + '`'
                if i < len(duty_list) - 1:
                    t_map += ', \n'
                qrinfo = 'Konum bilgisi için karekodu mobil cihazınıza okutunuz.'
                dctm.subdict_update(html_temp_dict, 'QRINFOHOLDER','value', qrinfo)
                dctm.subdict_update(html_temp_dict, 'IDHOLDER', 'value', str(i-my))

            dctm.subdict_update(html_temp_dict, 'ECZHOLDER', 'value', t_ecz)
            dctm.subdict_update(html_temp_dict, 'ADDRHOLDER', 'value', t_adr)
            dctm.subdict_update(html_temp_dict, 'TELHOLDER', 'value', t_tel)
            dctm.subdict_update(html_temp_dict, 'QRHOLDER', 'value', t_qr)
            dctm.subdict_update(html_temp_dict, 'MAPLISTH', 'value', t_map,
                                method='add')

            # For every place holder in HTMLBody non-template keys
            #
            t_dict = dctm.dict_get(html_temp_dict, 'HTMLBody')
            t_keys = dctm.dict_keys_get(t_dict, ['template', 'value','NRECZ',
                                                 'MAPH'])
            for key in t_keys:

                html_lib.Fill_Templates(t_dict[key], method='add')

            # Add footer pharmacies (Aslı Eczanesi, Ayhan Eczanesi ve Ali Eczanesi)
            #
            t_foot = row[0]
            if ((len(duty_list) > 1) and (len(duty_list) - 1 <= i)):
                t_foot = ' ve ' + t_foot
            elif (len(duty_list) > 2 and (len(duty_list) - 2 > i)):
                t_foot = t_foot + ', '
            else:
                t_foot = t_foot
            dctm.subdict_update(html_temp_dict, 'ECZLERH', 'value', t_foot, method='add')


        t_dict = dctm.subdict_update(html_temp_dict, 'CAPSH', 'value',
                                  trstr.TR_Case_Change(PHAR_NAME_PRINT, case='upper'))
        t_dict = dctm.subdict_update(html_temp_dict, 'DATEH', 'value', tomorrow_str)
        t_dict = dctm.subdict_update(html_temp_dict, 'HOURH', 'value', time_opening)
        t_dict = dctm.subdict_update(html_temp_dict, 'ECZMY', 'value', PHAR_NAME_PRINT)
        t_dict = dctm.subdict_update(html_temp_dict, 'CITYH', 'value', CITY_NAME)

        html_page = html_lib.Fill_Templates(html_temp_dict)

        return html_lib.Create_HTML_Page(html_page, file_path)
