# -*- coding: utf-8 -*-
"""
Created on Thu Oct 10 14:58:42 2019

@author: omemi
"""

import locale
import pathlib
import os
import sys

import datetime
import sched
import shlex
import subprocess
import time
#import re

#from bs4 import BeautifulSoup
#import pandas
import pygit2
import requests
import urllib


# Custom imports
#
from duty_center import DutyCenter, Webpage, Pharmacy, Screen

import ver_control as vc

import logger as lg
from scripter import Sh_Script_Write
import webdrive as wd
import file_manager as fm
import html_lib
import qr_maker as qrm
import string_manager as strm
import tr_string as trstr
import conf as cf
from city_names import city_names
import dict_manager as dctm
import schalendar as sk

##############
### Predefines
##############

# Local of the program
#
locale.setlocale(locale.LC_ALL, "tr_TR")

# System Platform Dependant Configurations
#
if sys.platform.startswith('win32'):
    HEADER = {
            "User-Agent": 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) '
            'AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 '
            'Safari/537.36'
            }
    DRIVER_PATH = os.path.join('drivers','win10','chromedriver.exe')
elif sys.platform.startswith('linux'):
    HEADER = {
            "User-Agent": 'Mozilla/5.0 (X11; Linux armv7l) '
            'AppleWebKit/537.36 (KHTML, like Gecko) '
            'Raspbian Chromium/72.0.3626.121 '
            'Chrome/72.0.3626.121 Safari/537.36'
            }
    DRIVER_PATH = os.path.join('drivers','raspi','chromedriver')
else:
    HEADER = None
    DRIVER_PATH = None

# Is Debug
#
DEBUG = 0
if len(sys.argv) > 1:
    for i in sys.argv:
        if i == 'debug':
            DEBUG = 1
            break

DIR_LOG = os.path.realpath('.log')

CONFS_TEMPLATE = {
    "graphics": {
        "ads": "no",
        "daytime": "no",
        "theme": "dpurple"
    },

    "user": {
        "city": "",         # 01 .. 81
        "district": "",
        "name": "",
        "type": "eczane"          # "eczane", "hastane"
    }
}

CONFS_TYPE_TEMPLATE = {
    "eczane": {
        "time_opening": "8.30",
        "time_closing": "18.15"
    },

    "hastane": {
        "time_opening": "8.30",
        "time_closing": "17.00"
    }
}

DUTY_PATH = os.path.abspath(os.path.join('html','nobetci_eczaneler.html'))
DUTY_TEMPLATE = os.path.abspath(os.path.join('html','template','html_template.html'))

SPLASH_PATH = os.path.abspath(os.path.join('html','splash.html'))
SPLASH_TEMPLATE = os.path.abspath(os.path.join('html','template','splash_template.html'))

AD_PATH = os.path.abspath(os.path.join('html','screensaver.html'))
AD_TEMPLATE =  os.path.abspath(os.path.join('html','template','gif_template.html'))

MEDIA_DIR = os.path.abspath(os.path.join('resimler'))

TIME_LIST = ['8.30', '9.00', '9.30', '10.00','12.00', '17.00',
              '17.30','18.00', '18.25','18.31','19.00']

# Check Frozen state for self execution command
#
if getattr(sys, 'frozen', False):
    # running in a bundle
    SELF_PATH = sys.executable
    SELF_PATH = "'" + SELF_PATH + "'"
    SELF_EXE = '' + SELF_PATH
else:
    # running live
    SELF_PATH = os.path.realpath(__file__)
    SELF_PATH = "'" + SELF_PATH + "'"
    SELF_EXE = 'python3 ' + SELF_PATH

##############
# Functions  #
##############
def Debug_Print(msg):
    if DEBUG:
        print(msg)

def Get_PIDS(service):
    upt_args = ''
    if DEBUG:
        upt_args += 'debug'

    # Get webbrowser PIDs
    #
    drv_pid = str(service.process.pid)
    upt_args += ' ' + drv_pid

    scr_path = os.path.abspath(os.path.join('..', 'get_pid.sh'))
    t_str = "ps -o ppid= -o pid= -A | awk '$1 == " + drv_pid +   \
            "{print $2}'"
    pid_lines = ['#! /bin/bash',
                 'sleep 3s',
                 'currentscript="$0"',
                 'function finish {',
                 'shred -u ${currentscript};',
                 '}',
                 t_str,
                 'sleep 1s',
                 'trap finish EXIT'
                ]
    cmd_pid = Sh_Script_Write(scr_path, pid_lines)
    result = subprocess.run(cmd_pid, stdout=subprocess.PIPE)
    res_str = result.stdout.decode()
    drv_list = []
    drv_list = res_str.strip().split()
    if len(drv_list):
        for i in drv_list:
            if i.isdigit():
                upt_args += ' ' + i

    Debug_Print('Driver pids are: ' + str(drv_pid))
    Debug_Print('Browser pids are: ' + str(drv_list))
    return upt_args

def My_Waiter():
    return

##############################################################################

def URL_Encoder(url):
     return urllib.parse.quote(url, encoding='ASCII')

def Maps_Geo_to_Encoded_URL(geolocation):
     maps_url_base = 'https://www.google.com/maps/search/?api=1&query='
     return URL_Encoder(maps_url_base + geolocation)

def Google_QR(inp):
    # Variables for QR URL Generation
    #
    qr_url_head = 'https://chart.googleapis.com/chart?cht=qr&chl='
    if inp.startswith('http'):
        qr_url_body = URL_Encoder(inp)
    else:
        qr_url_body = inp
    qr_url_end = '&chs=180x180&choe=UTF-8&chld=L|2'
    return qr_url_head + qr_url_body + qr_url_end

##############################################################################

#####################
# Global Variables  #
#####################

service_started = False

# Dependant files and folders list
#
file_dep_list = ['.conf/settings.conf',
                 'drivers/raspi/chromedriver',
                 'html/resources/eflash.gif',
                 'html/resources/get_date.js',
                 'html/resources/styles.css',
                 'html/resources/nulshock_bd.ttf',
                 'html/resources/3d_bg.svg',
                 'html/resources/3d_ecz.png',
                 'html/resources/3d_clk.png',
                 'html/resources/3d_duty.png',
                 'html/resources/osm-banner.png',
                 'html/template/gif_template.html',
                 'html/template/splash_template.html',
                 'html/template/html_template.html']
folder_dep_list = ['database',
                   '.conf']

# Temporary folders list
#
folder_temp_list = ['.temp', '.log', 'html/resources/qr', 'html/resources/map']

############
### MAIN
############
if __name__ == '__main__':
    try:
        # Initialize Remote Webdriver Service
        #
        service = wd.Service(DRIVER_PATH)
        service.start()
        service_started = True

        # Get the launch day of the information extractor and the day after
        #
        dt_today = datetime.datetime.today()
        dt_tomorrow = dt_today + datetime.timedelta(days=1)
        tomorrow_str = datetime.datetime.strftime(dt_tomorrow, '%d %B %Y')

        # Set working directory to git root folder
        #

        if sys.platform.startswith('linux'):
            result = subprocess.run(['git', 'rev-parse', '--show-toplevel'],    \
                                    stdout=subprocess.PIPE)
            dir_root = result.stdout.decode().strip()
        elif sys.platform.startswith('win32'):
            dir_root = os.getcwd()

        # Run TODO.py
        #
        path = pathlib.Path('.temp/todo.py')
        if path.exists() == True:
            subprocess.run(shlex.split('python3 ".temp/todo.py"'))
            time.sleep(1)
            subprocess.run(shlex.split('rm ".temp/todo.py"'))

        # Check necessary files and folders for the program to run
        #
        if not(fm.Files_Check(file_dep_list)):
            raise FileNotFoundError('Missing dependant files!')
        fm.Folders_Check(folder_temp_list, create=True)

        # Open the browser fullscreen for map screenshot
        #
        opts = ["--disable-infobars",
                "--window-size=1270,809"]
        webdriver_map = wd.Webdriver(service=service, options=opts)
        webdriver_map.driver.set_window_position(-2000,-2000)

        # Open the main browser
        #
        opts = ["--start-fullscreen", "--disable-infobars"]
        webdriver_main = wd.Webdriver(service=service, options=opts)
        time.sleep(3)

#        webdriver_map.driver.minimize_window()


        # Kill the processes from last run
        #
        if len(sys.argv) > 1:
            idx_args = 0
            for i in sys.argv:
                if (idx_args != 0) and (i.isdigit() == True):
                    subprocess.run(['kill', '-9', str(i)])
                idx_args += 1

    ### CONFIGURATIONS ###

        # Convert Configuration Template to OrderedDict
        #
        confs_template = dctm.dict_to_ordered(CONFS_TEMPLATE)

        ### Get Configurations
        #
        confs = cf.Config_Get(os.path.join('.conf', 'settings.conf'), 'tr')

        # Check Configurations with Template
        #
        is_config_ok = cf.Config_Check(confs._sections, confs_template)

        # If configs are updated, write the new ones to the file
        #
        if not(is_config_ok):
            f_obj = open(os.path.join('.conf', 'settings.conf'), "w", encoding="iso-8859-9")
            confs.write(f_obj)
            f_obj.close()

        confs_type = cf.Config_Get(os.path.join('database', 'type.conf'), 'tr')
        confs_template = dctm.dict_to_ordered(CONFS_TYPE_TEMPLATE)
        is_config_ok = cf.Config_Check(confs_type._sections, confs_template)

#        if not(is_config_ok):
#            f_obj = open(os.path.join('database', 'type.conf'), "w", encoding="iso-8859-9")
#            confs.write(f_obj)
#            f_obj.close()

        cf_duty_class = confs.get('user', 'type')

        is_daytime = bool(confs.getboolean('graphics', 'daytime'))

        time_opening = confs_type.get(cf_duty_class, "time_opening")
        time_closing = confs_type.get(cf_duty_class, "time_closing")

        # Check whether the database is available
        #
        if not(fm.Files_Check(['database/' +
                        str(confs.get('user', 'city')) + '_eczaneler.xlsx'])):
            raise FileNotFoundError('Missing dependant database!')


        # Copy settings into Duty Center
        #
        duty_center = DutyCenter(Pharmacy(confs.get('user','name'),
                                          confs.get('user', 'city'),
                                          city_names[0][confs.get('user', 'city')],
                                          confs.get('user', 'district')),
                                 Screen(DUTY_PATH,
                                        confs.get('graphics', 'theme'),
                                        bool(confs.getboolean('graphics','ads'))),
                                 HEADER,
                                 cf_duty_class)

        # Create the schedule module to run the function
        #
        schedular = sk.Schedular(TIME_LIST, interval=5)

        while True:
            try:
                os.chdir(dir_root)
                ##### 1. UPDATE CHECK #####
                Debug_Print('-> Version_Check()')
                is_update = vc.Version_Check(DEBUG=DEBUG)
                Debug_Print('<- Version_Check()')
                if (is_update == True):
                    Debug_Print('-> Version_Update()')
                    upt_args = Get_PIDS(service)
                    upt_exe = SELF_EXE + ' ' + upt_args
                    vc.Version_Update(dir_root, upt_exe, DEBUG)
                    Debug_Print('<- Version_Update()')

                ##### 1. Get Pharmacies in Duty From Website #####
#                webpage = Webpage(URL, ATTR_LIST, HEADER)
                web_infos = duty_center.webpage.extract()

                ##### 2. Compare Pharmacy Names from Website and XLS Database #####
                is_changed = False
                if duty_center.comp_get_duty(web_infos):
                    is_changed = duty_center.manage_memory(tomorrow=dt_tomorrow)
                if is_changed:
                    ##### 2.1 Generate HTML
                    html_duty = duty_center.generate_duty_screen(DUTY_PATH,
                                                                 webdriver=webdriver_map)

                ##### 3. Check if we can show the duty screen
                if ((sk.Is_Time_Between(time_opening, time_closing) == is_daytime) or  \
#                    not(duty_center.screen.is_saver) or
                    sk.Is_Holiday()):
                    duty_center.screen.path = 'file://' + DUTY_PATH

                    duty_center.screen.active = 'duty'
                else:
                    duty_center.screen.path = duty_center.generate_ad_screen(MEDIA_DIR,
                                                                  AD_PATH,
                                                                  AD_TEMPLATE)
                    duty_center.screen.active = 'ad'
                    if duty_center.screen.path == None:
                        duty_center.screen.path = duty_center.generate_splash_screen(
                                                                     SPLASH_PATH,
                                                                     SPLASH_TEMPLATE)
                        duty_center.screen.active = 'splash'

                # Check if Refresh is needed
                #
                if duty_center.screen.active == duty_center.screen.prev and    \
                (not(is_changed) or duty_center.screen.active != 'duty'):
                    is_refresh = False
                else:
                    is_refresh = True

                duty_center.screen.prev = duty_center.screen.active

                # Refresh the browser with the active page
                #
                if (is_refresh):
                    webdriver_main.open(duty_center.screen.path)

                # Run schedular
                #
                tm_schedule = schedular.schedule()
                Debug_Print('Next loop in ' + str(int(tm_schedule - time.time())) + ' seconds.')

                s = sched.scheduler(time.time, time.sleep)
                s.enterabs(tm_schedule, 0, My_Waiter)
                s.run()

            except (requests.ConnectionError, requests.ConnectTimeout, \
            requests.exceptions.ConnectionError, requests.RequestException):
                lg.Exception_Log(DIR_LOG)
                time.sleep(20)
                pass
            except Exception as e:
                lg.Exception_Log(DIR_LOG)
                time.sleep(60)
                pass

    except Exception as e:
        print('|||' + str(e) + '|||')
        lg.Exception_Log(DIR_LOG)
        if service_started:
            service.stop()
        sys.exit(60)

